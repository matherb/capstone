This project is meant to be a simple School database GUI for recording class attendance and viewing information.
It was written by Boaz Mather in the Spring of 2022 for the Capstone of his Computer and Information Science BS degree.



This project was developed using Apache NetBeans IDE 12.4

This repository simply stores the directory of this project from NetBeans, primary code files are found at src/main/java/SchoolDBM.

The images folder stores image files for use by the program.


This project is built to integrate with a MySQL database server on the localhost of the machine running it (basicFrame.java may have some more useful information).
The files here do not set up that database.

However the Database files folder contains the schema file and table files of the database used.

There is also a Screenshots directory that contains some demo images of the program in use.
