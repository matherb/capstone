package SchoolDBM;

import javax.swing.*;
import java.awt.*;
import java.sql.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginPage extends basicFrame implements ActionListener{
    
    //OBJECTS
    JButton login_button = new JButton("Login");
    JButton reset_button = new JButton("Reset");
    
    JTextField user_id_field = new JTextField();
    JPasswordField user_password_field = new JPasswordField();
    
    JLabel user_id_label = new JLabel("userID:");
    JLabel user_password_label = new JLabel("password:");
    JLabel error_message_label = new JLabel("");
    
    //VARS
    ResultSet result;
    PreparedStatement query;
    Boolean username_correct;
    Boolean password_correct;

    
    LoginPage() throws Exception{
        
        //Initial layout stuff
        this.setSize(420,420);
        this.setLayout(null);
        this.getRootPane().setDefaultButton(login_button);              //The default button is activated when enter is pressed
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        
        //Prepare the query to grab the users and their passwords
        query = db.prepareStatement("select * from users");
        
        
        //Create Labels
        user_id_label.setBounds(50,100,75,25);
        user_password_label.setBounds(50,150,75,25);
        
        //Create text boxes
        user_id_field.setBounds(125,100,200,25);
        user_password_field.setBounds(125,150,200,25);
        
        //Create the buttons
        login_button.setBounds(125,200,100,25);
        login_button.setFocusable(false);
        login_button.addActionListener(this);
        
        reset_button.setBounds(225,200,100,25);
        reset_button.setFocusable(false);
        reset_button.addActionListener(this);
        
        //Create error label
        error_message_label.setBounds(125,250,250,35);
        error_message_label.setFont(new Font(null,Font.ITALIC,25));
        

        
        //Add everything to the frame
        this.add(login_button);
        this.add(reset_button);
        this.add(user_id_field);
        this.add(user_password_field);
        this.add(error_message_label);
        this.add(user_id_label);
        this.add(user_password_label);
        
        this.setVisible(true);
    }

    
    //BUTTON HANDELING
    @Override
    public void actionPerformed(ActionEvent e){
        
       //if Reset is pressed, reset text and error fields
       if(e.getSource()==reset_button){
           user_id_field.setText("");
           user_password_field.setText("");
           error_message_label.setText("");
           
           username_correct = false;
           password_correct = false;
       }
       
       
       //if Login is pressed
       if(e.getSource()==login_button){
           
            //grab text from the fields
            String user_id = user_id_field.getText();
            String user_password = String.valueOf(user_password_field.getPassword());
            
            
            //reset field check bools
            username_correct = false;
            password_correct = false;


            //////////////////
            //Un-commenting this bypasses having to have the correct login info, this was used during testing for speed in getting to later pages
            //this.dispose();
            //try{WelcomePage welcomePage = new WelcomePage(user_id);}catch(Exception ex){}
            ////////////////////////////////
           
            
           //parse the result to find if the login was successful
           try{
               
               //Grab the info from the database (this could be optimized to not fetch the data again every time the button is pressed, but maybe we don't want that)
               result = query.executeQuery();
               
                //search the whole users table
                while(result.next()){
                
                    //verify user id
                    if(result.getString(1).equals(user_id)){
                        username_correct = true;
                        
                        //verify password
                        if(result.getString(2).equals(user_password)){
                            password_correct = true;
                        }
                        
                        break;
                    }
                }
                
                if(!username_correct){ //username not found
                    error_message_label.setForeground(Color.red);
                    error_message_label.setText("User not found");
                    
                }else if(!password_correct){ //wrong password
                    error_message_label.setForeground(Color.red);
                    error_message_label.setText("Wrong password");
                    
                    
                }else{          //successful login
                    this.dispose();
                    WelcomePage welcomePage = new WelcomePage(user_id);
                }
                
           }catch(Exception ex){
               System.out.println("ERROR: Database could not be accessed for login.");
           } 
       }
    }
}
