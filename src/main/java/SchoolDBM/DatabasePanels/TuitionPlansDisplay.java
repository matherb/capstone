package SchoolDBM.DatabasePanels;

import javax.swing.*;
import java.awt.*;
import java.sql.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.File;
import SchoolDBM.ScrollListPanel;

public class TuitionPlansDisplay extends JPanel{
    
    //OBJECTS
    ScrollListPanel scroll;
    ScrollListPanel students_panel;
    Connection db;
    
    //QUERY
    PreparedStatement query;
    ResultSet result;
    
    //VARS
    List<JPanel> panel_list = new ArrayList<>();
    List<JTextField> info_fields = new ArrayList<>();
        
    TuitionPlansDisplay(Connection DB, ScrollListPanel Scroll){
        
        //stoer passed in data for use outside constructor
        db = DB;
        scroll = Scroll;
        
        //grab table from database
        try{
            query = db.prepareStatement("select * from tuition_plans",ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            result = query.executeQuery();
        }catch(Exception ex){System.out.println("ERROR: Tuition Plan querry failed.");}
        
        
        //create the list of panels and add them to the scroll bar
        createPlanPanelList();
        refreshScrollList(scroll, panel_list);
        
        
        //Initial layout stuff
        this.setLayout(new BorderLayout());
        
        
        
        //////////////////////////////////////////////////////////////////
        //        Panel for primary info display
        ///////////////////////////////////////////////////////////////
        
        //wrapper for middle panel
        JPanel wrapper = new JPanel();
        wrapper.setLayout(new BorderLayout());
        
        //panel creation
        JPanel mid_panel = new JPanel();
        mid_panel.setLayout(new FlowLayout(FlowLayout.LEADING,0,0));
        int mid_width = 600;
        
        
        //id panel
        JPanel id_panel = new JPanel();
        makeInfoPanel(id_panel,"Plan ID:","",mid_width,200);
        
        //price panel
        JPanel price_panel = new JPanel();
        makeInfoPanel(price_panel,"Price:","",mid_width,200);
        
        //classes per week panel
        JPanel cpw_panel = new JPanel();
        makeInfoPanel(cpw_panel,"Classes per Week:","",mid_width,200);
        
        //add all to middle panel
        mid_panel.add(id_panel);
        mid_panel.add(price_panel);
        mid_panel.add(cpw_panel);
        
        
        
        
        //////////////
        //edit panel
        JPanel edit_panel = new JPanel();
        edit_panel.setLayout(new FlowLayout(FlowLayout.CENTER,20,20));
        
        //button to enable editing
        JButton start_edit = new JButton("Edit");
        start_edit.setFocusable(false);
        start_edit.setPreferredSize(new Dimension(60,60));
        start_edit.addActionListener(new ActionListener(){
           @Override
           public void actionPerformed(ActionEvent e){
               
               //for each text field (starting after id)
               for (int i=1; i<info_fields.size(); i++){
                       //allow the editing of that field
                       info_fields.get(i).setEditable(true);
               }  
           }
        });
        
        //buton to save edit
        JButton save_edit = new JButton("Save Edits");
        save_edit.setFocusable(false);
        save_edit.setPreferredSize(new Dimension(120,60));
        save_edit.addActionListener(new ActionListener(){
           @Override
            public void actionPerformed(ActionEvent e){

                //for each text field
                for (int i=0; i<info_fields.size(); i++){
                    //disable editing
                    info_fields.get(i).setEditable(false);
                }  

                //make sure we have a valid id
                if(!info_fields.get(0).getText().equals("")){
                    try{
                        //construct new data set from text fields
                        String new_data = "cost = '" +             info_fields.get(1).getText() + "', "+
                                          "classes_per_week = '" + info_fields.get(2).getText() +  "'";
                        
                        //insert into database
                        PreparedStatement update = db.prepareStatement("update tuition_plans set "+new_data+" where (tuition_plan_id = "+info_fields.get(0).getText()+")");
                        update.execute();

                    }catch(Exception ex){System.out.println("ERROR: Data edit failed.");}
                }
            }
        });
        
        //add to editing panel
        edit_panel.add(start_edit);
        edit_panel.add(save_edit);
        
        
        //add to wrapper
        wrapper.add(mid_panel, BorderLayout.CENTER);
        wrapper.add(edit_panel, BorderLayout.SOUTH);
        
        
        
        
        //////////////////////////////////////////////////////////////////
        //        Panel for list of people who attended
        ///////////////////////////////////////////////////////////////
        
        //panel creation
        JPanel right_panel = new JPanel();
        right_panel.setLayout(new BorderLayout());
        right_panel.setPreferredSize(new Dimension(300,2000));
        
        //label for attendees scroll pane
        JLabel students_label = new JLabel("Students w/ this plan:");
        students_label.setPreferredSize(new Dimension(300,40));
        students_label.setFont(new Font("",Font.BOLD, 20));
        students_label.setVerticalAlignment(JLabel.CENTER);
        students_label.setHorizontalAlignment(JLabel.CENTER);
        
        //scroll panel to list recent classes
        students_panel = new ScrollListPanel(5000);
        
        //add all to right panel
        right_panel.add(students_label,BorderLayout.NORTH);
        right_panel.add(students_panel,BorderLayout.CENTER);
        
        
        //add everything
        this.add(wrapper, BorderLayout.CENTER);
        this.add(right_panel, BorderLayout.EAST);
    }
    
    
    //////////////////////////////////////
    //  FUNCTIONS
    
    //update the scroll pane
    private void refreshScrollList(ScrollListPanel scr_panel, List<JPanel> pan_list){
        
        scr_panel.clearList();
        
        for(int i=0;i<pan_list.size();i++){
            scr_panel.list.add(pan_list.get(i));
        }
        
        scr_panel.list.revalidate();
        scr_panel.list.repaint();
    }
    
    //a function to help generate the smaller info panels
    private void makeInfoPanel(JPanel panel, String label_text, String field_text, int panel_width, int field_width){
        
        //make panel
        panel.setLayout(new FlowLayout(FlowLayout.CENTER,0,0));
        panel.setPreferredSize(new Dimension(panel_width, 150));
        
        //label
        JLabel label = new JLabel(label_text);
        label.setPreferredSize(new Dimension(500,40));
        label.setFont(new Font("",Font.BOLD, 20));
        label.setVerticalAlignment(JLabel.CENTER);
        label.setHorizontalAlignment(JLabel.CENTER);
        
        //text field
        JTextField field = new JTextField(field_text);
        field.setPreferredSize(new Dimension(field_width,40));
        field.setFont(new Font("",Font.PLAIN, 30));
        field.setEditable(false);
        field.setBackground(Color.white);
        
        //add to panel
        panel.add(label);
        panel.add(field);
        
        //store text field for later access
        info_fields.add(field);
    }
    
    //create the list of panels to be displayed in the scroll pane
    private void createPlanPanelList(){
        try{
            //for each result
            while(result.next()){
                int current_row = result.getRow();          //store what row we are on for insertion into button's function
                
                //make panel
                panel_list.add(new JPanel());
                panel_list.get(panel_list.size()-1).setPreferredSize(new Dimension (300, 100));
                panel_list.get(panel_list.size()-1).setBorder(BorderFactory.createLineBorder(Color.black,1));
                panel_list.get(panel_list.size()-1).setLayout(new FlowLayout(FlowLayout.LEADING,0,0));
                panel_list.get(panel_list.size()-1).setBackground(Color.LIGHT_GRAY);
                
                //wrapper for the labels
                JPanel label_wrapper = new JPanel();
                label_wrapper.setLayout(new BorderLayout());
                label_wrapper.setBackground(Color.LIGHT_GRAY);
                
                //label for the date
                JLabel id_price = new JLabel("ID: " + result.getString(1) + "  Price: $" + result.getString(2));
                id_price.setVerticalAlignment(JLabel.CENTER);
                id_price.setHorizontalAlignment(JLabel.CENTER);
                id_price.setFont(new Font("",Font.BOLD,20));
                id_price.setPreferredSize(new Dimension(230,50));
                
                //label for the title (type of class)
                JLabel cpw = new JLabel("Classes per week: " +result.getString(3));
                cpw.setVerticalAlignment(JLabel.CENTER);
                cpw.setHorizontalAlignment(JLabel.CENTER);
                cpw.setFont(new Font("",Font.BOLD,20));
                cpw.setPreferredSize(new Dimension(230,50));
                
                label_wrapper.add(id_price, BorderLayout.NORTH);
                label_wrapper.add(cpw, BorderLayout.SOUTH);
                
                
                //wrapper for the view button
                JPanel button_wrapper = new JPanel();
                button_wrapper.setLayout(new FlowLayout(FlowLayout.CENTER,0,25));
                button_wrapper.setBackground(Color.LIGHT_GRAY);
                button_wrapper.setPreferredSize(new Dimension(50,100));
                

                //Button for viewing this student's full entry
                JButton view_button = new JButton();
                view_button.setIcon(new ImageIcon("images/mag.png"));
                view_button.setPreferredSize(new Dimension(50,50));
                view_button.setFocusable(false);
                view_button.setBorderPainted(false);
                view_button.addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent e){
                        int row = current_row;
                        
                        try{
                            //set the result back to where this button is
                            result.beforeFirst();
                            for(int i=0;i<row;i++){
                                result.next();
                            }
                            
                            //make sure field editing is off
                            for(int i=0;i<info_fields.size();i++){
                                info_fields.get(i).setEditable(false);
                            }
                            
                            //set the text of each data field again
                            info_fields.get(0).setText(result.getString(1));    //plan id
                            info_fields.get(1).setText(result.getString(2));    //Price
                            info_fields.get(2).setText(result.getString(3));    //Classes per week
                            
                            //refresh the attendees list
                            refreshScrollList(students_panel, createStudentPanelList());
                                
                        }catch(Exception ex){System.out.println("ERROR: Result processing failed for view button press.");}
                    }
                });
                
                button_wrapper.add(view_button);      //add the button to its panel
                
                
                //add to the panel
                panel_list.get(panel_list.size()-1).add(label_wrapper, BorderLayout.CENTER);
                panel_list.get(panel_list.size()-1).add(button_wrapper, BorderLayout.EAST);
            }
        }catch(Exception ex){System.out.println("ERROR: Faild to create list of Tuition Plan panels.");}
    }
    
    
    //create the list of panels to be displayed in the student scroll pane
    private List<JPanel> createStudentPanelList(){
        
        List<JPanel> st_panel_list = new ArrayList<>();
        
        try{
            
            //query
            PreparedStatement st_querry = db.prepareStatement("select * from students where tuition_plan_id = "+info_fields.get(0).getText());
            ResultSet st_result = st_querry.executeQuery();
            
            while(st_result.next()){
                
                st_panel_list.add(new JPanel());
                st_panel_list.get(st_panel_list.size()-1).setPreferredSize(new Dimension (300, 100));
                st_panel_list.get(st_panel_list.size()-1).setBorder(BorderFactory.createLineBorder(Color.black,1));
                st_panel_list.get(st_panel_list.size()-1).setLayout(new FlowLayout(FlowLayout.LEADING, 0, 0));
                st_panel_list.get(st_panel_list.size()-1).setBackground(Color.LIGHT_GRAY);

                //Image for label
                BufferedImage img = ImageIO.read(new File("images/"+st_result.getString(8)));
                ImageIcon img_resized = new ImageIcon(img.getScaledInstance(100,100,Image.SCALE_SMOOTH));
                
                //Label for student info
                String info = "<html>"+
                            "ID: " + st_result.getString(1) + "<br>" +
                            st_result.getString(2) + " " + st_result.getString(3) + "<br>" +
                            st_result.getString(4)+" Belt";
                
                JLabel student_info = new JLabel(info);
                student_info.setIcon(img_resized);
                student_info.setHorizontalTextPosition(JLabel.RIGHT);
                student_info.setVerticalTextPosition(JLabel.CENTER);
                student_info.setFont(new Font("",Font.PLAIN,15));
                student_info.setPreferredSize(new Dimension(230,100));

                //add to the panel
                st_panel_list.get(st_panel_list.size()-1).add(student_info);
            }
        }catch(Exception ex){System.out.println("ERROR: Student panel list creation failed (most likely querry error)");}
        
        return(st_panel_list);
    }
}
