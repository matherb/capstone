package SchoolDBM.DatabasePanels;

import javax.swing.*;
import java.awt.*;
import java.sql.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import SchoolDBM.ScrollListPanel;

public class ClassInstanceDisplay extends JPanel{
    
    //OBJECTS
    ScrollListPanel scroll;
    ScrollListPanel attendees_panel;
    Connection db;
    
    //QUERY
    PreparedStatement query;
    ResultSet result;
    
    //VARS
    List<JPanel> panel_list = new ArrayList<>();
    List<JTextField> info_fields = new ArrayList<>();
    
    ClassInstanceDisplay(Connection DB, ScrollListPanel Scroll){
        
        //store passed in data for use outside constructor
        db = DB;
        scroll = Scroll;
        
        //grab table from database
        try{
            query = db.prepareStatement("select * from class_instance order by class_instance_id desc",ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            result = query.executeQuery();
            
        }catch(Exception ex){System.out.println("ERROR: Class Instance query failed");}
        
        
        //create the list of panels and add them to the scroll bar
        createClassPanelList();
        refreshScrollList(scroll, panel_list);
        
        
        //Initial layout stuff
        this.setLayout(new BorderLayout());
        
        
        
        //////////////////////////////////////////////////////////////////
        //        Panel for primary info display
        ///////////////////////////////////////////////////////////////
        
        //panel creation
        JPanel mid_panel = new JPanel();
        mid_panel.setLayout(new FlowLayout(FlowLayout.LEADING,0,0));
        int mid_width = 400;
        
        
        //id panel
        JPanel id_panel = new JPanel();
        makeInfoPanel(id_panel,"Instance ID:","",mid_width,200);
        
        //date panel
        JPanel date_panel = new JPanel();
        makeInfoPanel(date_panel,"Date Recorded:","",mid_width,300);
        
        //template id panel
        JPanel temp_id_panel = new JPanel();
        makeInfoPanel(temp_id_panel,"Template ID:","",mid_width,200);
        
        //title panel
        JPanel title_panel = new JPanel();
        makeInfoPanel(title_panel,"Title:","",mid_width,325);
        
        //day of the week panel
        JPanel dotw_panel = new JPanel();
        makeInfoPanel(dotw_panel,"Day of the Week:","",mid_width,200);
        
        //time panel
        JPanel time_panel = new JPanel();
        makeInfoPanel(time_panel,"Time:","",mid_width,200);
        
        
        //add all to middle panel
        mid_panel.add(id_panel);
        mid_panel.add(date_panel);
        mid_panel.add(temp_id_panel);
        mid_panel.add(title_panel);
        mid_panel.add(dotw_panel);
        mid_panel.add(time_panel);
        
        
        
        //////////////////////////////////////////////////////////////////
        //        Panel for list of people who attended
        ///////////////////////////////////////////////////////////////
        
        //panel creation
        JPanel right_panel = new JPanel();
        right_panel.setLayout(new BorderLayout());
        right_panel.setPreferredSize(new Dimension(300,2000));
        
        //label for attendees scroll pane
        JLabel attendees_label = new JLabel("Attendees:");
        attendees_label.setPreferredSize(new Dimension(300,40));
        attendees_label.setFont(new Font("",Font.BOLD, 20));
        attendees_label.setVerticalAlignment(JLabel.CENTER);
        attendees_label.setHorizontalAlignment(JLabel.CENTER);
        
        //scroll panel to list recent classes
        attendees_panel = new ScrollListPanel(5000);
        
        //add all to right panel
        right_panel.add(attendees_label,BorderLayout.NORTH);
        right_panel.add(attendees_panel,BorderLayout.CENTER);
        
        
        
        
        //add everything
        this.add(mid_panel, BorderLayout.CENTER);
        this.add(right_panel, BorderLayout.EAST);
    }
    
    
    
    
    
    //////////////////////////////////////
    //  FUNCTIONS
    
    //update the scroll pane
    private void refreshScrollList(ScrollListPanel scr_panel, List<JPanel> pan_list){
        
        scr_panel.clearList();
        
        for(int i=0;i<pan_list.size();i++){
            scr_panel.list.add(pan_list.get(i));
        }
        
        scr_panel.list.revalidate();
        scr_panel.list.repaint();
    }
    
    
    
    //a function to help generate the smaller info panels
    private void makeInfoPanel(JPanel panel, String label_text, String field_text, int panel_width, int field_width){
        
        //layout stuff
        panel.setLayout(new FlowLayout(FlowLayout.CENTER,0,0));
        panel.setPreferredSize(new Dimension(panel_width, 150));
        
        ///label
        JLabel label = new JLabel(label_text);
        label.setPreferredSize(new Dimension(500,40));
        label.setFont(new Font("",Font.BOLD, 20));
        label.setVerticalAlignment(JLabel.CENTER);
        label.setHorizontalAlignment(JLabel.CENTER);
        
        //text field
        JTextField field = new JTextField(field_text);
        field.setPreferredSize(new Dimension(field_width,40));
        field.setFont(new Font("",Font.PLAIN, 30));
        field.setEditable(false);
        field.setBackground(Color.white);
        
        //add to the panel
        panel.add(label);
        panel.add(field);
        
        //add the text field to a list for future access
        info_fields.add(field);
    }    
    
    
    
    //create the list of panels to be displayed in the scroll pane
    private void createClassPanelList(){
        try{
            //for each result in the query
            while(result.next()){
                int current_row = result.getRow();
                PreparedStatement template_query = db.prepareStatement("select * from class_template where class_template_id = "+result.getString(3));
                ResultSet template_result = template_query.executeQuery();
                template_result.next();
         
                //make the panel
                panel_list.add(new JPanel());
                panel_list.get(panel_list.size()-1).setPreferredSize(new Dimension (300, 100));
                panel_list.get(panel_list.size()-1).setBorder(BorderFactory.createLineBorder(Color.black,1));
                panel_list.get(panel_list.size()-1).setLayout(new FlowLayout(FlowLayout.LEADING,0,0));
                panel_list.get(panel_list.size()-1).setBackground(Color.LIGHT_GRAY);
                
                //wrapper panel for the labels
                JPanel label_wrapper = new JPanel();
                label_wrapper.setLayout(new BorderLayout());
                label_wrapper.setBackground(Color.LIGHT_GRAY);
                
                //label for the date
                JLabel class_date = new JLabel("Date: "+result.getString(2));
                class_date.setVerticalAlignment(JLabel.CENTER);
                class_date.setHorizontalAlignment(JLabel.CENTER);
                class_date.setFont(new Font("",Font.BOLD,15));
                class_date.setPreferredSize(new Dimension(230,50));
                
                //label for the title (type of class)
                JLabel class_title = new JLabel(template_result.getString(4));
                class_title.setVerticalAlignment(JLabel.TOP);
                class_title.setHorizontalAlignment(JLabel.CENTER);
                class_title.setFont(new Font("",Font.BOLD,20));
                class_title.setPreferredSize(new Dimension(230,50));
                
                //add to wrapper
                label_wrapper.add(class_date, BorderLayout.NORTH);
                label_wrapper.add(class_title, BorderLayout.SOUTH);
                
                
                //wrapper for the view button
                JPanel button_wrapper = new JPanel();
                button_wrapper.setLayout(new FlowLayout(FlowLayout.CENTER,0,25));
                button_wrapper.setBackground(Color.LIGHT_GRAY);
                button_wrapper.setPreferredSize(new Dimension(50,100));
                

                //Button for viewing this student's full entry
                JButton view_button = new JButton();
                view_button.setIcon(new ImageIcon("images/mag.png"));
                view_button.setPreferredSize(new Dimension(50,50));
                view_button.setFocusable(false);
                view_button.setBorderPainted(false);
                view_button.addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent e){
                        int row = current_row;
                        ResultSet temp_res = template_result;
                        
                        try{
                            //set the result back to where this button is
                            result.beforeFirst();
                            for(int i=0;i<row;i++){
                                result.next();
                            }
                            
                            //set the text of each data field again
                            info_fields.get(0).setText(result.getString(1));    //instance id
                            info_fields.get(1).setText(result.getString(2));    //date recorded
                            info_fields.get(2).setText(result.getString(3));    //template id
                            
                            info_fields.get(3).setText(temp_res.getString(4));  //title
                            info_fields.get(4).setText(temp_res.getString(2));  //day of the week
                            info_fields.get(5).setText(temp_res.getString(3));  //time
                            
                            
                            //refresh the attendees list
                            refreshScrollList(attendees_panel, createStudentPanelList());

                        }catch(Exception ex){System.out.println("ERROR: Result processing failed on view button press.");}
                    }
                });
                
                button_wrapper.add(view_button);      //add the button to its panel
                
                
                //add to the panel
                
                panel_list.get(panel_list.size()-1).add(label_wrapper, BorderLayout.CENTER);
                panel_list.get(panel_list.size()-1).add(button_wrapper, BorderLayout.EAST);
            }
        }catch(Exception ex){System.out.println("ERROR: Class template query failed");}
    }
    

    
    //create the list of panels to be displayed in the student scroll pane
    private List<JPanel> createStudentPanelList(){
        
        List<JPanel> st_panel_list = new ArrayList<>();
        
        try{
            
            //query
            PreparedStatement st_querry = db.prepareStatement("select a.student_id, a.first_name, a.last_name, a.rank, a.photo_name from students a, student_in_class b where a.student_id = b.student_id and class_instance_id = "+info_fields.get(0).getText());
            ResultSet st_result = st_querry.executeQuery();
            
            while(st_result.next()){
                
                //make panel
                st_panel_list.add(new JPanel());
                st_panel_list.get(st_panel_list.size()-1).setPreferredSize(new Dimension (300, 100));
                st_panel_list.get(st_panel_list.size()-1).setBorder(BorderFactory.createLineBorder(Color.black,1));
                st_panel_list.get(st_panel_list.size()-1).setLayout(new FlowLayout(FlowLayout.LEADING, 0, 0));
                st_panel_list.get(st_panel_list.size()-1).setBackground(Color.LIGHT_GRAY);

                //Image for label
                BufferedImage img = ImageIO.read(new File("images/"+st_result.getString(5)));
                ImageIcon img_resized = new ImageIcon(img.getScaledInstance(100,100,Image.SCALE_SMOOTH));
                
                //Label for student info
                String info = "<html>"+
                            "ID: " + st_result.getString(1) + "<br>" +
                            st_result.getString(2) + " " + st_result.getString(3) + "<br>" +
                            st_result.getString(4)+" Belt";
                
                JLabel student_info = new JLabel(info);
                student_info.setIcon(img_resized);
                student_info.setHorizontalTextPosition(JLabel.RIGHT);
                student_info.setVerticalTextPosition(JLabel.CENTER);
                student_info.setFont(new Font("",Font.PLAIN,15));
                student_info.setPreferredSize(new Dimension(230,100));

                //add to the panel
                st_panel_list.get(st_panel_list.size()-1).add(student_info);
            }
        }catch(Exception ex){System.out.println("Error: Query failed for student panel list");}
        
        return(st_panel_list);
    }
}
