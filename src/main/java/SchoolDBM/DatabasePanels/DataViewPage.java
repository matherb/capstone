package SchoolDBM.DatabasePanels;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import SchoolDBM.HomePanel;
import SchoolDBM.ScrollListPanel;
import SchoolDBM.basicFrame;

public class DataViewPage extends basicFrame{
    
    /////////////
    // OBJECTS
    JPanel main_wrapper;
    JPanel data_display = new JPanel();
    JLabel tab_label;
    ScrollListPanel list_panel;
    
    ///////////////
    // VARIABLES
    String display_type = "Please choose an option above...";
    
    public DataViewPage()throws Exception{
        
        //Initial layout stuff
        this.setSize(1920, 1080);
        this.setExtendedState(Frame.MAXIMIZED_BOTH);            //maximize window
        this.setLayout(new BorderLayout());
        
        
        
        
        /////////////////////////////////////
        // HOME PANEL SETUP
        Dimension button_size = new Dimension(150, 55);
        
        //Student view button
        JButton student_button = new JButton("Students");
        student_button.setPreferredSize(button_size);
        student_button.setFocusable(false);
        student_button.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                try{

                    display_type = "Students";
                    refreshDisplay();
                    
                }catch(Exception ex){System.out.println("ERROR: Database connection failed.");}
            }
        });
        
        //Recorded class view button
        JButton recorded_class_button = new JButton("Recorded Classes");
        recorded_class_button.setPreferredSize(button_size);
        recorded_class_button.setFocusable(false);
        recorded_class_button.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                try{

                    display_type = "Recorded Classes";
                    refreshDisplay();
                    
                }catch(Exception ex){System.out.println("ERROR: Database connection failed.");}
            }
        });
        
        //Class template view button
        JButton class_template_button = new JButton("Class Types");
        class_template_button.setPreferredSize(button_size);
        class_template_button.setFocusable(false);
        class_template_button.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                try{

                    display_type = "Class Types";
                    refreshDisplay();
                    
                }catch(Exception ex){System.out.println("ERROR: Database connection failed.");}
            }
        });
        
        //Tuition plans view button
        JButton tuition_plan_button = new JButton("Tuition Plans");
        tuition_plan_button.setPreferredSize(button_size);
        tuition_plan_button.setFocusable(false);
        tuition_plan_button.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                try{

                    display_type = "Tuition Plans";
                    refreshDisplay();
                    
                }catch(Exception ex){System.out.println("ERROR: Database connection failed.");}
            }
        });

        
        //Make home panel and add buttons
        HomePanel home_panel = new HomePanel(this);
        home_panel.left_panel.add(student_button);
        home_panel.left_panel.add(recorded_class_button);
        home_panel.left_panel.add(class_template_button);
        home_panel.left_panel.add(tuition_plan_button);
        
        
        
        
        
        ////////////////////////////////////////////////////////////////
        //  MAIN DATA VIEW
        
        //A label to say which tab we are on
        tab_label = new JLabel(display_type);
        tab_label.setPreferredSize(new Dimension(1000, 75));
        tab_label.setFont(new Font("",Font.PLAIN,50));
        tab_label.setForeground(Color.red);
        tab_label.setVerticalAlignment(JLabel.CENTER);
        tab_label.setHorizontalAlignment(JLabel.CENTER);
        tab_label.setBorder(BorderFactory.createLineBorder(Color.black,1));
        
        //A Panel object that will serve as the reference variable to whatever data panel we are viewing
        data_display = new JPanel();
        
        //make the entry list scroll pane
        list_panel = new ScrollListPanel(10000);
        
        //A Panel object that will serve as the reference variable to whatever data panel we are viewing
        data_display = new JPanel();
        
        
        //create wrapper panel and add elements
        main_wrapper = new JPanel();
        main_wrapper.setLayout(new BorderLayout());
        main_wrapper.add(tab_label, BorderLayout.NORTH);
        main_wrapper.add(list_panel, BorderLayout.WEST);
        main_wrapper.add(data_display, BorderLayout.CENTER);

        
        
        //Add everything to the frame
        this.add(home_panel,BorderLayout.NORTH);
        this.add(main_wrapper, BorderLayout.CENTER);
        this.setVisible(true);
    }
    
    
    
    ////////////////////////////
    //  FUNCTIONS
    ////////////////////////////
    
    
    //  A function to refresh the scroll panel and display when something changes
    private void refreshDisplay(){
        
        
        ///////////////////////////////////
        //  DISPLAY PANEL REFRESH
        
        //remove the previous panel
        main_wrapper.remove(data_display);
        
        //change it
        if(display_type.equals("Students")){                    //student panel
            data_display = new StudentDisplay(db, list_panel);
            
        }else if(display_type.equals("Class Types")){           //class template panel
            data_display = new ClassTemplateDisplay(db, list_panel);
            
        }else if(display_type.equals("Recorded Classes")){      //class instance panel
            data_display = new ClassInstanceDisplay(db, list_panel);
            
        }else if(display_type.equals("Tuition Plans")){         //tuition plans panel
            data_display = new TuitionPlansDisplay(db, list_panel);
            
        }else{                                                  //Default blank if none of the above
            data_display = new JPanel();
        }
        
        //now add it back
        main_wrapper.add(data_display, BorderLayout.CENTER);
        
        
        
        //refresh visuals of main display panel
        data_display.revalidate();
        data_display.repaint();
        
        //refresh the header label
        tab_label.setText(display_type);
        tab_label.setForeground(Color.black);

        //refresh the scroll panel
        list_panel.list.revalidate();
        list_panel.list.repaint();
    }
}
