package SchoolDBM.DatabasePanels;

import javax.swing.*;
import java.awt.*;
import java.sql.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import SchoolDBM.ScrollListPanel;

public class ClassTemplateDisplay extends JPanel{
    
    //OBJECTS
    ScrollListPanel scroll;
    Connection db;
    
    //QUERY
    PreparedStatement query;
    ResultSet result;
    
    //VARS
    List<JPanel> panel_list = new ArrayList<>();
    List<JTextField> info_fields = new ArrayList<>();
    
    ClassTemplateDisplay(Connection DB, ScrollListPanel Scroll){
        
        //store passed in vars for access outside constructor
        db = DB;
        scroll = Scroll;
        
        //grab table from database
        try{
            query = db.prepareStatement("select * from class_template",ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            result = query.executeQuery();
            
        }catch(Exception ex){System.out.println("ERROR: Class Template query failed.");}
        
        
        //create the list of panels and add them to the scroll bar
        createTemplatePanelList();
        refreshScrollList(scroll, panel_list);
        
        
        //Initial layout stuff
        this.setLayout(new BorderLayout());
        
        
        
        //////////////////////////////////////////////////////////////////
        //        Panel for primary info display
        ///////////////////////////////////////////////////////////////
        
        //wrapper for middle panel
        JPanel wrapper = new JPanel();
        wrapper.setLayout(new BorderLayout());
        
        //panel creation
        JPanel mid_panel = new JPanel();
        mid_panel.setLayout(new FlowLayout(FlowLayout.LEADING,0,0));
        int mid_width = 500;
        
        
        //id panel
        JPanel id_panel = new JPanel();
        makeInfoPanel(id_panel,"Template ID:","",mid_width,100);
        
        //day panel
        JPanel day_panel = new JPanel();
        makeInfoPanel(day_panel,"Day of the Week:","",mid_width,200);
        
        //time panel
        JPanel time_panel = new JPanel();
        makeInfoPanel(time_panel,"Time:","",mid_width,250);
        
        //title panel
        JPanel title_panel = new JPanel();
        makeInfoPanel(title_panel,"Title:","",mid_width,350);
        
        //Subtitle panel
        JPanel sub_title_panel = new JPanel();
        makeInfoPanel(sub_title_panel,"Subtittle","",mid_width+100,550);
        

        //add all to middle panel
        mid_panel.add(id_panel);
        mid_panel.add(day_panel);
        mid_panel.add(time_panel);
        mid_panel.add(title_panel);
        mid_panel.add(sub_title_panel);
        
        
        
        
        //////////////
        //edit panel
        JPanel edit_panel = new JPanel();
        edit_panel.setLayout(new FlowLayout(FlowLayout.CENTER,20,20));
        
        //button to enable editing
        JButton start_edit = new JButton("Edit");
        start_edit.setFocusable(false);
        start_edit.setPreferredSize(new Dimension(60,60));
        start_edit.addActionListener(new ActionListener(){
           @Override
           public void actionPerformed(ActionEvent e){
               
               //for each text field (starting after id)
               for (int i=1; i<info_fields.size(); i++){
                       //allow the editing of that field
                       info_fields.get(i).setEditable(true);
               }  
           }
        });
        
        //buton to save edit
        JButton save_edit = new JButton("Save Edits");
        save_edit.setFocusable(false);
        save_edit.setPreferredSize(new Dimension(120,60));
        save_edit.addActionListener(new ActionListener(){
           @Override
            public void actionPerformed(ActionEvent e){

                //for each text field
                for (int i=0; i<info_fields.size(); i++){
                    //disable editing
                    info_fields.get(i).setEditable(false);
                }  

                //make sure we have a valid  id
                if(!info_fields.get(0).getText().equals("")){
                    try{
                        //construct new data set from text fields
                        String new_data = "day = '" +      info_fields.get(1).getText() + "', "+
                                          "time = '" +     info_fields.get(2).getText() + "', "+
                                          "title = '" +    info_fields.get(3).getText() + "', "+
                                          "sub_text = '" + info_fields.get(4).getText() + "'";
                        
                        //insert into database
                        PreparedStatement update = db.prepareStatement("update class_template set "+new_data+" where (class_template_id = "+info_fields.get(0).getText()+")");
                        update.execute();

                    }catch(Exception ex){System.out.println("ERROR: Data edit failed.");}
                }
            }
        });
        
        //add to editing panel
        edit_panel.add(start_edit);
        edit_panel.add(save_edit);
        
        
        
        
        //add everything
        wrapper.add(mid_panel, BorderLayout.CENTER);
        wrapper.add(edit_panel, BorderLayout.SOUTH);
        
        this.add(wrapper, BorderLayout.CENTER);
    }
    
    
    
    //////////////////////////////////////
    //  FUNCTIONS
    
    //update the scroll pane
    private void refreshScrollList(ScrollListPanel scr_panel, List<JPanel> pan_list){
        
        scr_panel.clearList();
        
        for(int i=0;i<pan_list.size();i++){
            scr_panel.list.add(pan_list.get(i));
        }
        
        scr_panel.list.revalidate();
        scr_panel.list.repaint();
    }
    
    
    
    //a function to help generate the smaller info panels
    private void makeInfoPanel(JPanel panel, String label_text, String field_text, int panel_width, int field_width){
        
        //layout stuff
        panel.setLayout(new FlowLayout(FlowLayout.CENTER,0,0));
        panel.setPreferredSize(new Dimension(panel_width, 150));
        
        //label
        JLabel label = new JLabel(label_text);
        label.setPreferredSize(new Dimension(500,40));
        label.setFont(new Font("",Font.BOLD, 20));
        label.setVerticalAlignment(JLabel.CENTER);
        label.setHorizontalAlignment(JLabel.CENTER);
        
        //text field
        JTextField field = new JTextField(field_text);
        field.setPreferredSize(new Dimension(field_width,40));
        field.setFont(new Font("",Font.PLAIN, 30));
        field.setEditable(false);
        field.setBackground(Color.white);
        
        //add to panel
        panel.add(label);
        panel.add(field);
        
        //store text field for future access
        info_fields.add(field);
    } 
    
    //create the list of panels to be displayed in the scroll pane
    private void createTemplatePanelList(){
        try{
            //for each result of the query
            while(result.next()){
                int current_row = result.getRow();          //store what row we are on for insertion into the button's function
         
                //make panel
                panel_list.add(new JPanel());
                panel_list.get(panel_list.size()-1).setPreferredSize(new Dimension (300, 100));
                panel_list.get(panel_list.size()-1).setBorder(BorderFactory.createLineBorder(Color.black,1));
                panel_list.get(panel_list.size()-1).setLayout(new FlowLayout(FlowLayout.LEADING,0,0));
                panel_list.get(panel_list.size()-1).setBackground(Color.LIGHT_GRAY);
                
                //wrapper for the labels
                JPanel label_wrapper = new JPanel();
                label_wrapper.setLayout(new BorderLayout());
                label_wrapper.setBackground(Color.LIGHT_GRAY);
                
                //label for the title
                JLabel class_title = new JLabel(result.getString(4));
                class_title.setVerticalAlignment(JLabel.CENTER);
                class_title.setHorizontalAlignment(JLabel.CENTER);
                class_title.setFont(new Font("",Font.BOLD,20));
                class_title.setPreferredSize(new Dimension(230,30));
                
                //label for dotw
                JLabel class_date = new JLabel(result.getString(2) + "s @");
                class_date.setVerticalAlignment(JLabel.CENTER);
                class_date.setHorizontalAlignment(JLabel.CENTER);
                class_date.setFont(new Font("",Font.BOLD,15));
                class_date.setPreferredSize(new Dimension(230,30));
                
                //label for the time 
                JLabel class_time = new JLabel(result.getString(3));
                class_time.setVerticalAlignment(JLabel.CENTER);
                class_time.setHorizontalAlignment(JLabel.CENTER);
                class_time.setFont(new Font("",Font.BOLD,20));
                class_time.setPreferredSize(new Dimension(230,30));
                
                label_wrapper.add(class_title, BorderLayout.NORTH);
                label_wrapper.add(class_date, BorderLayout.CENTER);
                label_wrapper.add(class_time, BorderLayout.SOUTH);
                
                
                
                //wrapper for the view button
                JPanel button_wrapper = new JPanel();
                button_wrapper.setLayout(new FlowLayout(FlowLayout.CENTER,0,25));
                button_wrapper.setBackground(Color.LIGHT_GRAY);
                button_wrapper.setPreferredSize(new Dimension(50,100));
                

                //Button for viewing this student's full entry
                JButton view_button = new JButton();
                view_button.setIcon(new ImageIcon("images/mag.png"));
                view_button.setPreferredSize(new Dimension(50,50));
                view_button.setFocusable(false);
                view_button.setBorderPainted(false);
                view_button.addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent e){
                        int row = current_row;
                        
                        try{
                            //set the result back to where this button is
                            result.beforeFirst();
                            for(int i=0;i<row;i++){
                                result.next();
                            }
                            
                            //make sure field editing is off
                            for(int i=0;i<info_fields.size();i++){
                                info_fields.get(i).setEditable(false);
                            }
                            
                            //set the text of each data field again
                            info_fields.get(0).setText(result.getString(1));    //template id
                            info_fields.get(1).setText(result.getString(2));    //dotw
                            info_fields.get(2).setText(result.getString(3));    //time
                            info_fields.get(3).setText(result.getString(4));  //title
                            info_fields.get(4).setText(result.getString(5));  //subtitle

                        }catch(Exception ex){System.out.println("ERROR: Result processing failed on view button press.");}
                    }
                });
                
                button_wrapper.add(view_button);      //add the button to its panel
                
                
                //add to the panel
                panel_list.get(panel_list.size()-1).add(label_wrapper, BorderLayout.CENTER);
                panel_list.get(panel_list.size()-1).add(button_wrapper, BorderLayout.EAST);
            }
        }catch(Exception ex){System.out.println("ERROR: Result processing failed for template panel list.");}
    }
}
