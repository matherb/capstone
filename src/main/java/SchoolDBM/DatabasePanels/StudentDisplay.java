package SchoolDBM.DatabasePanels;

import javax.swing.*;
import java.awt.*;
import java.sql.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.ArrayList;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.File;
import SchoolDBM.ScrollListPanel;

public class StudentDisplay extends JPanel{

    //OBJECTS
    Connection db;
    ScrollListPanel scroll;
    ScrollListPanel recent_class_panel;
    JLabel image_label;
    
    
    //QUERY
    PreparedStatement query;
    ResultSet result;
        
    //VARS
    int left_width;
    List<JPanel> panel_list = new ArrayList<>();
    List<JTextField> info_fields = new ArrayList<>();
    String[] info_arr = new String[] {"","","","","","","",""};
    
    //strings to hold relevant student info
    /*
        info_arr[0] = student_id        ----> info_fields[1]
        info_arr[1] = first_name        ----> info_fields[3]
        info_arr[2] = last_name         ----> info_fields[4]
        info_arr[3] = rank              ----> info_fields[5]
        info_arr[4] = dob               ----> info_fields[6]
        info_arr[5] = start_date        ----> info_fields[7]
        info_arr[6] = tuition_plan_id   ----> info_fields[2]
        info_arr[7] = photo_name        ----> info_fields[0]
    */
    
    // ^^^^This mess^^^^ should be organized, but it works for now
    
    
    StudentDisplay(Connection DB, ScrollListPanel Scroll){
        
        //store passed in vars for use outside constructor
        db = DB;
        scroll = Scroll;
        
        //grab student table from database
        try{
            query = db.prepareStatement("select * from students",ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            result = query.executeQuery();
            
        }catch(Exception ex){System.out.println("ERROR: Student query failed.");}
        
        //create the list of panels and add them to the scroll bar
        createStudentPanelList();
        refreshScrollList(scroll, panel_list);

        
        
        //Initial layout stuff
        this.setLayout(new BorderLayout());
        
        
        //wrapper for left and middle panels
        JPanel wrapper = new JPanel();
        wrapper.setLayout(new BorderLayout());
        
        ///////////////////////////////////////////////////////////////////
        //      Panel for pic, photo name (and invoices?)
        ///////////////////////////////////////////////////////////////////
        
        //panel creation
        JPanel left_panel = new JPanel();
        left_width = 300;
        left_panel.setLayout(new FlowLayout(FlowLayout.LEADING,0,0));
        left_panel.setPreferredSize(new Dimension(left_width,2000));
        
        
        //Label for student image
        image_label = new JLabel();
        image_label.setPreferredSize(new Dimension(left_width,left_width));
        try{
            //get the image
            BufferedImage img = ImageIO.read(new File("images/"+info_arr[7]));
            ImageIcon img_resized = new ImageIcon(img.getScaledInstance(left_width,left_width,Image.SCALE_SMOOTH));
            
            image_label.setIcon(img_resized);
            image_label.setText("");
        }catch(Exception ex){                       // if the image processing failed
            image_label.setText("IMAGE NOT FOUND");
            image_label.setVerticalAlignment(JLabel.CENTER);
            image_label.setHorizontalAlignment(JLabel.CENTER);
        }
        
        
        //Panel for the image name
        JPanel img_name_panel = new JPanel();
        makeInfoPanel(img_name_panel,"Photo Name:",info_arr[7],left_width,200);
 
        
        //add all to the left panel
        left_panel.add(image_label);
        left_panel.add(img_name_panel);

        
        
        
        ////////////////////////////////////////////////////////////////////////////
        //      Panel for primary info display
        //////////////////////////////////////////////////////////////////////////
        
        //panel creation
        JPanel mid_panel = new JPanel();
        mid_panel.setLayout(new FlowLayout(FlowLayout.LEADING,0,0));
        int mid_width = 400;    //out of 1020 ish
        
        
        //Panel for ID
        JPanel id_panel = new JPanel();
        makeInfoPanel(id_panel,"Student ID:",info_arr[0],mid_width,200);
        
        //Panel for Tuition ID
        JPanel tid_panel = new JPanel();
        makeInfoPanel(tid_panel,"Tuition Plan ID:",info_arr[6],mid_width,200);
        
        //Panel for First Name
        JPanel fn_panel = new JPanel();
        makeInfoPanel(fn_panel,"First Name:",info_arr[1],mid_width,200);
        
        //Panel for Last Name
        JPanel ln_panel = new JPanel();
        makeInfoPanel(ln_panel,"Last Name:",info_arr[2],mid_width,200);
        
        //Panel for rank
        JPanel rank_panel = new JPanel();
        makeInfoPanel(rank_panel,"Rank:",info_arr[3],mid_width,200);
        
        //Panel for DOB
        JPanel dob_panel = new JPanel();
        makeInfoPanel(dob_panel,"Date of Birth:",info_arr[4],mid_width,200);
        
        //Panel for Start date
        JPanel sd_panel = new JPanel();
        makeInfoPanel(sd_panel,"Start Date:",info_arr[5],mid_width,200);
  
        
        
        //add all to middle panel
        mid_panel.add(id_panel);
        mid_panel.add(tid_panel);
        mid_panel.add(fn_panel);
        mid_panel.add(ln_panel);
        mid_panel.add(rank_panel);
        mid_panel.add(dob_panel);
        mid_panel.add(sd_panel);
        
        
        //////////////
        //edit panel
        JPanel edit_panel = new JPanel();
        edit_panel.setLayout(new FlowLayout(FlowLayout.CENTER,20,20));
        
        //button to enable editing
        JButton start_edit = new JButton("Edit");
        start_edit.setFocusable(false);
        start_edit.setPreferredSize(new Dimension(60,60));
        start_edit.addActionListener(new ActionListener(){
           @Override
           public void actionPerformed(ActionEvent e){
               //for each text field
               for (int i=0; i<info_fields.size(); i++){
                   //except id
                   if(i!=1){
                       //allow the editing of that field
                       info_fields.get(i).setEditable(true);
                   }
               }  
           }
        });
        
        //buton to save edit
        JButton save_edit = new JButton("Save Edits");
        save_edit.setFocusable(false);
        save_edit.setPreferredSize(new Dimension(120,60));
        save_edit.addActionListener(new ActionListener(){
           @Override
            public void actionPerformed(ActionEvent e){

                //for each text field
                for (int i=0; i<info_fields.size(); i++){
                    //disable editing
                    info_fields.get(i).setEditable(false);
                }  

                //make sure we have a valid  student id
                if(!info_fields.get(1).getText().equals("")){
                    try{
                        //construct new data set from text fields
                        String new_data = "first_name = '" +    info_fields.get(3).getText() + "', "+
                                          "last_name = '" +     info_fields.get(4).getText() + "', "+
                                          "st.rank = '" +       info_fields.get(5).getText() + "', "+
                                          "dob = '" +           info_fields.get(6).getText() + "', "+
                                          "start_date = '" +    info_fields.get(7).getText() + "', "+
                                          "tuition_plan_id = "+ info_fields.get(2).getText() + ", "+
                                          "photo_name = '" +    info_fields.get(0).getText() + "'";
                        
                        
                        //insert into database
                        PreparedStatement update = db.prepareStatement("update students st set "+new_data+" where (student_id = "+info_fields.get(1).getText()+")");
                        update.execute();

                    }catch(Exception ex){System.out.println("ERROR: Data edit failed.");}
                    
                    
                    
                    //update the image
                    try{
                        BufferedImage img = ImageIO.read(new File("images/"+info_fields.get(0).getText()));
                        ImageIcon img_resized = new ImageIcon(img.getScaledInstance(left_width,left_width,Image.SCALE_SMOOTH));
                        image_label.setIcon(img_resized);
                        image_label.setText("");
                        
                    }catch(Exception ex){                       // if the image processing failed
                        image_label.setText("IMAGE NOT FOUND");
                        image_label.setVerticalAlignment(JLabel.CENTER);
                        image_label.setHorizontalAlignment(JLabel.CENTER);
                    }
                }
            }
        });
        
        //add to editing panel
        edit_panel.add(start_edit);
        edit_panel.add(save_edit);
        
        
        
        
        //add to wrapper
        wrapper.add(left_panel, BorderLayout.WEST);
        wrapper.add(mid_panel, BorderLayout.CENTER);
        wrapper.add(edit_panel, BorderLayout.SOUTH);
        
        
        
        /////////////////////////////////////////////////////////////////////
        //      Panel for the list of recently attended classes
        ////////////////////////////////////////////////////////////////////
        
        //panel creation
        JPanel right_panel = new JPanel();
        right_panel.setLayout(new BorderLayout());
        right_panel.setPreferredSize(new Dimension(300,2000));
        
        //label for the recent class scroll pane
        JLabel recent_class_label = new JLabel("Recent Classes:");
        recent_class_label.setPreferredSize(new Dimension(300,40));
        recent_class_label.setFont(new Font("",Font.BOLD, 20));
        recent_class_label.setVerticalAlignment(JLabel.CENTER);
        recent_class_label.setHorizontalAlignment(JLabel.CENTER);
        
        //scroll panel to list recent classes
        recent_class_panel = new ScrollListPanel(5000);
        
        //add all to right panel
        right_panel.add(recent_class_label,BorderLayout.NORTH);
        right_panel.add(recent_class_panel,BorderLayout.CENTER);
        
        
        
        
        //////////////////////////////////////////////////////////////////
        //Add everything
        this.add(right_panel, BorderLayout.EAST);
        this.add(wrapper, BorderLayout.CENTER);
    }
    
    
    //////////////////////////////////////
    //  FUNCTIONS

    //update the scroll pane
    private void refreshScrollList(ScrollListPanel scr_panel, List<JPanel> pan_list){
        
        scr_panel.clearList();
        
        for(int i=0;i<pan_list.size();i++){
            scr_panel.list.add(pan_list.get(i));
        }
        
        scr_panel.list.revalidate();
        scr_panel.list.repaint();
    }
    
    
    
    //a function to help generate the smaller info panels
    private void makeInfoPanel(JPanel panel, String label_text, String field_text, int panel_width, int field_width){
        
        //make panel
        panel.setLayout(new FlowLayout(FlowLayout.CENTER,0,0));
        panel.setPreferredSize(new Dimension(panel_width, 150));
        
        //label
        JLabel label = new JLabel(label_text);
        label.setPreferredSize(new Dimension(500,40));
        label.setFont(new Font("",Font.BOLD, 20));
        label.setVerticalAlignment(JLabel.CENTER);
        label.setHorizontalAlignment(JLabel.CENTER);
        
        //text field
        JTextField field = new JTextField(field_text);
        field.setPreferredSize(new Dimension(field_width,40));
        field.setFont(new Font("",Font.PLAIN, 30));
        field.setEditable(false);
        field.setBackground(Color.white);
        
        //add to panel
        panel.add(label);
        panel.add(field);
        
        //store text field for later access
        info_fields.add(field);
    }
    
    
    
    //create the list of panels to be displayed in the scroll pane
    private void createStudentPanelList(){
        try{
            //for every entry in the result
            while(result.next()){
                int current_row = result.getRow();      //store what row we are on for insertion into button's function
         
                //make panel
                panel_list.add(new JPanel());
                panel_list.get(panel_list.size()-1).setPreferredSize(new Dimension (300, 100));
                panel_list.get(panel_list.size()-1).setBorder(BorderFactory.createLineBorder(Color.black,1));
                panel_list.get(panel_list.size()-1).setLayout(new FlowLayout(FlowLayout.LEADING, 0, 0));
                panel_list.get(panel_list.size()-1).setBackground(Color.LIGHT_GRAY);
            
                //Image for label
                BufferedImage img = ImageIO.read(new File("images/"+result.getString(8)));
                ImageIcon img_resized = new ImageIcon(img.getScaledInstance(100,100,Image.SCALE_SMOOTH));

                //Label for student info
                String info = "<html>"+
                            "ID: " + result.getString(1) + "<br>" +
                            result.getString(2) + " " + result.getString(3) + "<br>" +
                            result.getString(4)+" Belt";
                
                JLabel student_info = new JLabel(info);
                student_info.setIcon(img_resized);
                student_info.setHorizontalTextPosition(JLabel.RIGHT);
                student_info.setVerticalTextPosition(JLabel.CENTER);
                student_info.setFont(new Font("",Font.PLAIN,15));
                student_info.setPreferredSize(new Dimension(230,100));

                //Button for viewing this student's full entry
                JButton view_button = new JButton();
                view_button.setIcon(new ImageIcon("images/mag.png"));
                view_button.setPreferredSize(new Dimension(50,50));
                view_button.setFocusable(false);
                view_button.setBorderPainted(false);
                view_button.addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent e){
                        int row = current_row;
                        
                        try{
                            //set the result back to where this button is
                            result.beforeFirst();
                            for(int i=0;i<row;i++){
                                result.next();
                            }
                            
                            //change the strings
                            for(int i=0;i<info_arr.length;i++){
                                info_arr[i] = result.getString(i+1);
                            }
                            
                            //make sure field editing is off
                            for(int i=0;i<info_fields.size();i++){
                                info_fields.get(i).setEditable(false);
                            }
                            
                            //Change the text of the fields    (see chart in variables at the top ^^^)
                            info_fields.get(0).setText(info_arr[7]);        //photo name
                            info_fields.get(1).setText(info_arr[0]);        //student id
                            info_fields.get(2).setText(info_arr[6]);        //tuition id
                            info_fields.get(3).setText(info_arr[1]);        //first name
                            info_fields.get(4).setText(info_arr[2]);        //last name
                            info_fields.get(5).setText(info_arr[3]);        //rank
                            info_fields.get(6).setText(info_arr[4]);        //dob
                            info_fields.get(7).setText(info_arr[5]);        //start date
                                
                            
                            //Change the picture being displayed
                            BufferedImage img = ImageIO.read(new File("images/"+info_arr[7]));
                            ImageIcon img_resized = new ImageIcon(img.getScaledInstance(left_width,left_width,Image.SCALE_SMOOTH));
                            image_label.setIcon(img_resized);
                            image_label.setText("");
                            
                            
                            //Refresh the recent class scroll pane
                            refreshScrollList(recent_class_panel, createClassPanelList());
                            
                        }catch(Exception ex){System.out.println("ERROR: Result processing failed on view button press.");}
                    }
                });

                //add to the panel
                panel_list.get(panel_list.size()-1).add(student_info);
                panel_list.get(panel_list.size()-1).add(view_button);
            }
        }catch(Exception ex){System.out.println("ERROR: Student panel list failed to be made (image error or result processing error).");}
    }
    
    
    
    //create the list of panels to be displayed in the recent classes scroll pane
    private List<JPanel> createClassPanelList(){
        
        List<JPanel> cl_panel_list = new ArrayList<>();
        
        try{
            
            PreparedStatement cl_querry = db.prepareStatement("select inst.*, temp.title "+
                                                              "from class_instance inst, student_in_class sic, class_template temp "+
                                                              "where inst.class_instance_id = sic.class_instance_id "+
                                                                "and inst.class_template_id = temp.class_template_id "+
                                                                "and sic.student_id = " + info_fields.get(1).getText() + " "+
                                                              "order by inst.class_instance_id desc");
            ResultSet cl_result = cl_querry.executeQuery();
            
            //for each row returned by the query
            while(cl_result.next()){
                
                //create a panel
                cl_panel_list.add(new JPanel());
                cl_panel_list.get(cl_panel_list.size()-1).setPreferredSize(new Dimension (300, 100));
                cl_panel_list.get(cl_panel_list.size()-1).setBorder(BorderFactory.createLineBorder(Color.black,1));
                cl_panel_list.get(cl_panel_list.size()-1).setLayout(new FlowLayout(FlowLayout.LEADING,0,0));
                cl_panel_list.get(cl_panel_list.size()-1).setBackground(Color.LIGHT_GRAY);

                
                //label for the date
                JLabel class_date = new JLabel("Date: "+cl_result.getString(2));
                class_date.setVerticalAlignment(JLabel.CENTER);
                class_date.setHorizontalAlignment(JLabel.CENTER);
                class_date.setFont(new Font("",Font.BOLD,15));
                class_date.setPreferredSize(new Dimension(300,50));
                
                //label for the title (type of class)
                JLabel class_title = new JLabel(cl_result.getString(4));
                class_title.setVerticalAlignment(JLabel.TOP);
                class_title.setHorizontalAlignment(JLabel.CENTER);
                class_title.setFont(new Font("",Font.BOLD,20));
                class_title.setPreferredSize(new Dimension(300,50));

                
                
                //add to the panel
                cl_panel_list.get(cl_panel_list.size()-1).add(class_date, BorderLayout.NORTH);
                cl_panel_list.get(cl_panel_list.size()-1).add(class_title, BorderLayout.SOUTH);
            }
        }catch(Exception ex){System.out.println("ERROR: Class Panel list query failed.");}
        
        return(cl_panel_list);
    }
}
