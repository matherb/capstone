package SchoolDBM.ClassRecordPanels;

import java.awt.*;
import SchoolDBM.HomePanel;
import SchoolDBM.basicFrame;

public class ChooseClassPage extends basicFrame{
    
    public ChooseClassPage()throws Exception{
        
        //Initial layout stuff
        this.setSize(1920, 1080);
        this.setExtendedState(Frame.MAXIMIZED_BOTH);            //maximize window
        this.setLayout(new BorderLayout());
        
        
        //Add home panel
        HomePanel home_panel = new HomePanel(this);
        
        //Add the main panel
        ChooseClassPanel choose_panel = new ChooseClassPanel(this, db);
        
        
        //Add everything to the frame
        this.add(home_panel,BorderLayout.NORTH);
        this.add(choose_panel,BorderLayout.CENTER);
        
        this.setVisible(true);
    }
}
