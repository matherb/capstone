package SchoolDBM.ClassRecordPanels;

import SchoolDBM.*;

import javax.swing.*;
import java.awt.*;
import java.sql.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.ArrayList;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.File;

public class ClassRecordPage extends basicFrame implements ActionListener{
    
    //OBJECTS
    JButton add_student_button;
    JButton end_rec_button;
    JTextField student_id_field;
    JLabel id_error_label;
    ScrollListPanel student_list_panel;
    
    //VARS
    String template_id;
    List<JPanel> panel_list = new ArrayList<>();
    List<String> id_list = new ArrayList<>();
    
    
    public ClassRecordPage(String temp_id)throws Exception{
        
        //initial layout stuff
        this.setSize(1920, 1080);
        this.setExtendedState(Frame.MAXIMIZED_BOTH);
        this.setLayout(new BorderLayout());
        
        //store template id so I can access it in the Action Listener
        template_id=temp_id;
        
        //Grab information about the passed in class_template
        PreparedStatement query;
        query = db.prepareStatement("select * from class_template where class_template_id = "+template_id);
        ResultSet result = query.executeQuery();
        result.next();
        
        
        //Create a panel for the information and registration field in the center
        JPanel center_panel = new JPanel();
        center_panel.setLayout(new FlowLayout());

        //Class info labels
        JLabel day_label = new JLabel(result.getString(2));             //date
        day_label.setFont(new Font("", Font.PLAIN,50));
        day_label.setPreferredSize(new Dimension(3000,300));
        day_label.setVerticalAlignment(JLabel.BOTTOM);
        day_label.setHorizontalAlignment(JLabel.CENTER);
         
        JLabel time_label = new JLabel(result.getString(3));            //time
        time_label.setFont(new Font("",Font.PLAIN,50));
        time_label.setPreferredSize(new Dimension(3000,80));
        time_label.setVerticalAlignment(JLabel.BOTTOM);
        time_label.setHorizontalAlignment(JLabel.CENTER);

        JLabel descript_label = new JLabel(result.getString(4));        //description
        descript_label.setFont(new Font("",Font.PLAIN,50));
        descript_label.setPreferredSize(new Dimension(3000,100));
        descript_label.setVerticalAlignment(JLabel.CENTER);
        descript_label.setHorizontalAlignment(JLabel.CENTER);

        
        
        
        
        
        //Create a panel to hold the textfield, its label and button
        JPanel enter_id_panel = new JPanel();
        enter_id_panel.setPreferredSize(new Dimension(3000,100));
        enter_id_panel.setLayout(new FlowLayout());
   
        //Label for the student id field
        JLabel enter_id_label = new JLabel("Enter Student ID: ");
        enter_id_label.setFont(new Font("",Font.PLAIN,30));
        
        //Box to enter student id
        student_id_field = new JTextField();
        student_id_field.setPreferredSize(new Dimension(300,50));
        student_id_field.setFont(new Font("",Font.PLAIN,30));
        
        //Button to add student to registry
        add_student_button = new JButton("+");
        add_student_button.setFont(new Font("",Font.BOLD,30));
        add_student_button.setFocusable(false);
        add_student_button.addActionListener(this);

        
        
        //Label for id error text
        id_error_label = new JLabel("");
        id_error_label.setPreferredSize(new Dimension(3000, 200));
        id_error_label.setFont(new Font("",Font.ITALIC,25));
        id_error_label.setForeground(Color.red);
        id_error_label.setVerticalAlignment(JLabel.TOP);
        id_error_label.setHorizontalAlignment(JLabel.CENTER);
        id_error_label.setOpaque(true);

        
 
        
        
        //Create a panel for the End Recording Button
        JPanel end_rec_panel = new JPanel();
        end_rec_panel.setPreferredSize(new Dimension(3000,200));
        end_rec_panel.setLayout(new FlowLayout());
        
        //Create the End Recording Button
        end_rec_button = new JButton("End Recording");
        end_rec_button.setFont(new Font("",Font.BOLD,25));
        end_rec_button.setPreferredSize(new Dimension(300, 75));
        end_rec_button.setFocusable(false);
        end_rec_button.addActionListener(this);
        
        
        
        
        
        //create a home panel
        /*
        TODO:   add the home panel but make it create a pop up window when pressed
                this window needs to prompt for the saving or discarding of the recording
        */
        //HomePanel home_panel = new HomePanel(this);
        
        
        
        //Create scrollable list panel (pass in the length cuz I cant figure out dynamic length)
        student_list_panel = new ScrollListPanel(10000);
        
        
        
        //Add Everything
        enter_id_panel.add(enter_id_label);                 //to the studentID panel
        enter_id_panel.add(student_id_field);
        enter_id_panel.add(add_student_button);
        
        end_rec_panel.add(end_rec_button);                  //to the end recording panel
        
        center_panel.add(day_label);                        //to the center panel
        center_panel.add(time_label);
        center_panel.add(descript_label);
        center_panel.add(enter_id_panel);
        center_panel.add(id_error_label);
        center_panel.add(end_rec_panel);

        this.add(student_list_panel,BorderLayout.WEST);     //to the frame
        this.add(center_panel,BorderLayout.CENTER);
        //this.add(home_panel,BorderLayout.NORTH);
        this.getRootPane().setDefaultButton(add_student_button);
        this.setVisible(true);
    }

    ////////////////////////////////////
    //Buttons Function
    @Override
    public void actionPerformed(ActionEvent e) {
        try{
            PreparedStatement query;
            ResultSet result;
            
            //Try to add another student to the recording
            if(e.getSource() == add_student_button){
                
                //Try to grab the student from the database
                query = db.prepareStatement("select * from students where student_id = "+student_id_field.getText());
                result = query.executeQuery();
                

                //check if that student was actually there and has not been enterd in already
                if(result.next()){                                  //result.next returns false if it is empty
                    
                    //if the student is already recorded
                    if(id_list.contains(result.getString(1))){
                        id_error_label.setText("Student already recorded");
                        student_id_field.setText("");
                        
                        
                    //if it was not already recorded
                    }else{
                    
                        //add the student to the list
                        id_list.add(student_id_field.getText());        //store the id


                        //clear error and text fields
                        id_error_label.setText("");
                        student_id_field.setText("");

                        
                        //////////////////////////////////////////////////////////////////
                        //Make the panel
                        panel_list.add(new JPanel());                   
                        panel_list.get(panel_list.size()-1).setPreferredSize(new Dimension (300, 100));
                        panel_list.get(panel_list.size()-1).setBorder(BorderFactory.createLineBorder(Color.black,1));
                        panel_list.get(panel_list.size()-1).setLayout(new FlowLayout(FlowLayout.LEADING, 0, 0));
                        panel_list.get(panel_list.size()-1).setBackground(Color.LIGHT_GRAY);
                        
                        //Image for label
                        BufferedImage img = ImageIO.read(new File("images/"+result.getString(8)));
                        ImageIcon img_resized = new ImageIcon(img.getScaledInstance(100,100,Image.SCALE_SMOOTH));
                        
                        //Label for student info
                        String info = "<html>"+result.getString(2)+" "+result.getString(3)+"<br>"+result.getString(4)+" Belt";
                        JLabel student_name = new JLabel(info);
                        student_name.setIcon(img_resized);
                        student_name.setHorizontalTextPosition(JLabel.RIGHT);
                        student_name.setVerticalTextPosition(JLabel.CENTER);
                        student_name.setFont(new Font("",Font.PLAIN,20));
                        student_name.setPreferredSize(new Dimension(300,100));
                        
                        
                        /*
                        TODO:
                            Add a button to each panel that removes it from the panel list
                            and the list of ids that need to be recorded
                        */
                        
                        //add to the panel
                        panel_list.get(panel_list.size()-1).add(student_name);

                        
                        //update the student list panel
                        refreshStudentPanel();
                    }
  
                }else{  //if it was not in the database
                    
                    id_error_label.setText("Student not in database");
                    student_id_field.setText("");
                }
            }
        
        
            
            
            
            //Submit the recording to the database
            if(e.getSource() == end_rec_button){
                
                //ensure it can only be pressed once
                end_rec_button.setEnabled(false);

                
                //create an instance of the passed in class template
                query = db.prepareStatement("insert into class_instance (class_template_id) values("+template_id+")");
                query.execute();
                
                //Grab the id of the newest instance (the one we just made)
                query = db.prepareStatement("select class_instance_id from class_instance order by class_instance_id desc limit 1");
                result = query.executeQuery();
                result.next();
                

                //Create student_in_class records
                for(int i=0;i<id_list.size();i++){
                    query = db.prepareStatement("insert into student_in_class (student_id, class_instance_id) values(" + id_list.get(i) + ", " + result.getString(1) + ")");
                    query.execute();
                }
                
                
                //Exit this frame and return to Welcome Page
                this.dispose();
                try{WelcomePage welcomePage = new WelcomePage("Sensei");}catch(Exception ex){System.out.println("ERROR: Database access failed.");}
            } 
        }catch(Exception ex){System.out.println("ERROR: Query failed.");}
    }
    
    
    
    /////////////////////////////////////////////////////////////////////////////////////
    //This should be called to refresh the list on the side every time it is updated
    private void refreshStudentPanel(){
        
        student_list_panel.clearList();
        
        for(int i=0;i<panel_list.size();i++){                   //add all panels in the list
            student_list_panel.list.add(panel_list.get(i));
        }
        
        student_list_panel.list.revalidate();                   //refresh the display
        student_list_panel.list.repaint();  
    }
}