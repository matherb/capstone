package SchoolDBM.ClassRecordPanels;

import javax.swing.*;
import java.awt.*;
import java.sql.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class ChooseClassPanel extends JPanel{
    
    //OBJECTS
    JFrame host_frame;
    Connection db;          //because this is a panel, it needs the connection passed to it
    
    public ChooseClassPanel(JFrame host, Connection database)throws Exception{
        
        //variable storage for access outside the constructor
        host_frame = host;
        db = database;
        
        //Initial layout stuff
        this.setLayout(new BorderLayout());
        
        
        //Create a heading label
        JLabel heading = new JLabel("Choose a class to record");
        heading.setFont(new Font("", Font.PLAIN,50));
        heading.setPreferredSize(new Dimension(100,150));
        heading.setVerticalAlignment(JLabel.CENTER);
        heading.setHorizontalAlignment(JLabel.CENTER);
        
        
        //Create the center panel for the week table
        JPanel week_panel = new JPanel();
        week_panel.setLayout(new GridLayout());
        week_panel.setBorder(BorderFactory.createLineBorder(Color.black,11));
        
        //Create, fill and add a list of panels, one for each day
        List<JPanel> day_panels = new ArrayList<>();
        makeDayPanels(day_panels);
        for(int i=0; i<day_panels.size(); i++){
            week_panel.add(day_panels.get(i));
        }
        
        
        //Create filler panels to get the center one the right size
        JPanel filler_left = new JPanel();
        filler_left.setPreferredSize(new Dimension(200,0));
        JPanel filler_right = new JPanel();
        filler_right.setPreferredSize(new Dimension(200,0));
        JPanel filler_bottom = new JPanel();
        filler_bottom.setPreferredSize(new Dimension(0,50));
        
        
        
        //Add everything
        this.add(filler_left,BorderLayout.WEST);
        this.add(filler_right,BorderLayout.EAST);
        this.add(filler_bottom,BorderLayout.SOUTH);
        
        this.add(heading,BorderLayout.NORTH);
        this.add(week_panel,BorderLayout.CENTER);
        this.setOpaque(true);
    }
    
    
    ///////////////////////////////////
    //FUNCTIONS
    
    //Create the vertical panels for each day of the week
    private void makeDayPanels(List<JPanel> list) throws Exception{
        
        //vars
        String[] days = {"Mon", "Tues", "Wed", "Thurs", "Fri", "Sat", "Sun"};
        List<JLabel> labels = new ArrayList<>();
        List<JButton> buttons = new ArrayList<>();
        
        //Get the class-template data
        PreparedStatement query = db.prepareStatement("select * from class_template");
        ResultSet result;
        
        
        
        
        //make and do this for each day of the week
        for(int i=0; i<7; i++){
            
            //fetch the table again
            result = query.executeQuery();          //should be optimized to reset to the beginning of the result rather than fetched again
            
            //make the panel, set layout and border
            list.add(new JPanel());
            list.get(i).setLayout(new FlowLayout(FlowLayout.CENTER,0,0));
            list.get(i).setBorder(BorderFactory.createLineBorder(Color.black,5));
            
            //the heading label
            labels.add(new JLabel(days[i]));
            labels.get(i).setPreferredSize(new Dimension(300,75));
            labels.get(i).setBorder(BorderFactory.createLineBorder(Color.black,2));
            labels.get(i).setFont(new Font("", Font.PLAIN,50));
            labels.get(i).setVerticalAlignment(JLabel.CENTER);
            labels.get(i).setHorizontalAlignment(JLabel.CENTER);
            list.get(i).add(labels.get(i));
            

            //loop to make the buttons
            while(result.next()){
                
                //Only make and add the button if the current row's day matches the day column we are in
                if((result.getString(2).equals("Monday")   && i==0) ||
                   (result.getString(2).equals("Tuesday")  && i==1) ||
                   (result.getString(2).equals("Wednesday")&& i==2) ||
                   (result.getString(2).equals("Thursday") && i==3) ||
                   (result.getString(2).equals("Friday")   && i==4) ||
                   (result.getString(2).equals("Saturday") && i==5) ||
                   (result.getString(2).equals("Sunday")   && i==6) ){

                    //create and format new button
                    /*
                    TODO:   Display more information in these buttons
                    */
                    buttons.add(new JButton(result.getString(4)+" Class"));
                    buttons.get(buttons.size()-1).setPreferredSize(new Dimension(204,125));
                    buttons.get(buttons.size()-1).setFocusable(false);
                    
                    //Get the class_template id
                    String template_id = result.getString(1);

                    //add the action listener, defining what the button does
                    buttons.get(buttons.size()-1).addActionListener(new ActionListener(){
                        @Override
                        public void actionPerformed(ActionEvent e){
                            try{
                                host_frame.dispose();
                                ClassRecordPage recordingPage = new ClassRecordPage(template_id);
                            }catch(Exception ex){System.out.println("ERROR: database connection failed.");}
                        }
                    });

                    //add the button to the panel we are on
                    list.get(i).add(buttons.get(buttons.size()-1));
                } 
            }
        }
    }
}
