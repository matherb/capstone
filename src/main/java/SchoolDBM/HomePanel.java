package SchoolDBM;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class HomePanel extends JPanel{
    
    //OBJECTS
    JButton home_button;
    public JPanel left_panel;
    
    public HomePanel(JFrame host_frame){
        
        //Initial layout stuff
        this.setPreferredSize(new Dimension(1000,90));
        this.setLayout(new BorderLayout());
        this.setBackground(Color.LIGHT_GRAY);
        
        //add a panel on the left side for host frames to access and add stuff to
        left_panel = new JPanel();
        left_panel.setLayout(new FlowLayout(FlowLayout.LEADING, 20, 20));
        left_panel.setBackground(Color.LIGHT_GRAY);
        
        //make a panel on the right side for the home button
        JPanel right_panel = new JPanel();
        right_panel.setLayout(new FlowLayout(FlowLayout.TRAILING, 25, 5));
        right_panel.setBackground(Color.LIGHT_GRAY);
        
        //Create home button
        home_button = new JButton("Home");
        home_button.setPreferredSize(new Dimension(80,80));
        home_button.setFocusable(false);
        
        //Have this button close the hosting frame and open the Welcome Page
        home_button.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                try{

                    host_frame.dispose();
                    WelcomePage welcomePage = new WelcomePage("Sensei");
                    
                }catch(Exception ex){System.out.println("ERROR: Database connection failed.");}
            }
        });

        
        
        //Add everything
        right_panel.add(home_button);
        
        this.add(right_panel, BorderLayout.EAST);
        this.add(left_panel, BorderLayout.WEST);
    }
}
