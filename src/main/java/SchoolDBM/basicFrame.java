package SchoolDBM;

import javax.swing.*;
import java.sql.*;

//This is a basic window template that sets some universal settings and properties
//All frames should inherit from this
public class basicFrame extends JFrame{
    
    //the connection to the database
    protected Connection db;

    public basicFrame()throws Exception{
        ImageIcon logo = new ImageIcon("images/logo.jpg");    //grab the logo image from here
        this.setIconImage(logo.getImage());                    //Set the logo image for the top left corner
        this.setTitle("School Database Manager");
        
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    //Exit the program on close
        
        
        //Create the connection to the database
        db = DriverManager.getConnection("jdbc:mysql://localhost:3306/schooldb", "javaGUI", "password");
    }
}
