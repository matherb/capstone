package SchoolDBM;

import javax.swing.*;
import java.awt.*;

public class ScrollListPanel extends JPanel{
    
    public JPanel list = new JPanel();
    
    public ScrollListPanel(int length){
        
        //initial layout stuff
        this.setLayout(new BorderLayout());
        this.setPreferredSize(new Dimension(300,0));
        this.setBorder(BorderFactory.createEtchedBorder());
        
        
        //panel to hold the items of the list
        list.setLayout(new FlowLayout());
        list.setPreferredSize(new Dimension(290,length));
        
        
        //The scroll bar panel          
        /*
        TODO:   make it dynamically scale the length of the scrollable list 
                panel as needed (as opposed to setting its length statically to 10000
                in order to make it work)
        */
        JScrollPane scroll_bar = new JScrollPane();
        scroll_bar.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scroll_bar.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scroll_bar.setViewportView(list);

        
        //add everything
        this.add(scroll_bar,BorderLayout.CENTER);
    }
    
    //function to empty the scroll panel
    public void clearList(){
        Component[] components = this.list.getComponents();     //get an array of everying added to the panel
        
        for(int i=0; i<components.length; i++){                 //remove all panels added to it
            if(components[i] instanceof JPanel){
                this.list.remove(components[i]);
            }
        }
    }
}
