package SchoolDBM;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import SchoolDBM.ClassRecordPanels.ChooseClassPage;
import SchoolDBM.DatabasePanels.DataViewPage;

public class WelcomePage extends basicFrame implements ActionListener{
    
    //OBJECTS
    JButton class_record_button;
    JButton data_view_button;
    
    public WelcomePage(String userID)throws Exception{
        
        //Initial layout stuff
        this.setSize(1920, 1080);
        this.setExtendedState(Frame.MAXIMIZED_BOTH);            //maximize window
        this.setLayout(new BorderLayout());
        
        
        
        //Create label for this panel
        JLabel intro_label = new JLabel("Hello Sensei");        //I could make this display the username from login, but I would have to store that somewhere when comming back here from other frames
        intro_label.setFont(new Font("", Font.PLAIN,50));
        intro_label.setPreferredSize(new Dimension(100,300));
        intro_label.setVerticalAlignment(JLabel.CENTER);
        intro_label.setHorizontalAlignment(JLabel.CENTER);
        

        
        //Create panel for the buttons
        JPanel button_panel = new JPanel();
        button_panel.setPreferredSize(new Dimension(100, 100));
        button_panel.setLayout(new FlowLayout(FlowLayout.CENTER, 100, 50));

        //Create the buttons and add them to the panel
        class_record_button = new JButton("Record a Class");
        class_record_button.addActionListener(this);
        class_record_button.setPreferredSize(new Dimension(500,500));
        class_record_button.setFocusable(false);
        button_panel.add(class_record_button);    
        
        data_view_button = new JButton("View Database");
        data_view_button.addActionListener(this);
        data_view_button.setPreferredSize(new Dimension(500,500));
        data_view_button.setFocusable(false);
        button_panel.add(data_view_button);
        
        
        
        //add everything
        this.add(intro_label,BorderLayout.NORTH);
        this.add(button_panel,BorderLayout.CENTER);
        
        this.setVisible(true); 
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try{
            //if record a class is selected
            if(e.getSource() == class_record_button){
               this.dispose();
               ChooseClassPage classRecordPage = new ChooseClassPage();
            }

            //if view database is selected
            if(e.getSource() == data_view_button){
                this.dispose();
                DataViewPage dataViewPage = new DataViewPage();
            }
        }catch(Exception ex){System.out.println("ERROR: Database connection failed");}
    }
}
